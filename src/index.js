const path = require( "path" );

process.env.TS_NODE_PROJECT = path.join( __dirname, "../tsconfig.json" );

require( "ts-node" ).register( { files: true } );
require( "tsconfig-paths" ).register();

let mainFuncName = process.argv[ 2 ] || "main";
let mainFunc = require( "./app" )[ mainFuncName ];

if ( typeof mainFunc === "function" ) {
    mainFunc();
} else {
    throw new Error( `Invalid command: ${mainFuncName}` );
}