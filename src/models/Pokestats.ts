export interface Pokestats {
    attackIv: number;
    defenseIv: number;
    staminaIv: number;
    level: number;
}