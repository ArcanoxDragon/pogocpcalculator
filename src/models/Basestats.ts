export interface Basestats {
    attack: number;
    defense: number;
    stamina: number;
}