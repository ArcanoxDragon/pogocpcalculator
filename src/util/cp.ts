import { Basestats } from "@/models/Basestats";
import { Pokestats } from "@/models/Pokestats";
import { readCsvAsync } from "@/util/csv";

import path from "path";

const { pow, max, floor } = Math;

let cpMultipliers: Record<number, number>;

export async function getCpAsync( baseStats: Basestats, pokeStats: Pokestats ) {
    // CP = (Atk * Def^0.5 * Sta^0.5 * CPM^2) / 10, minimum 10

    let attack = baseStats.attack + pokeStats.attackIv;
    let defense = baseStats.defense + pokeStats.defenseIv;
    let stamina = baseStats.stamina + pokeStats.staminaIv;
    let cpm = await getCpMultiplierAsync( pokeStats.level );

    return floor( max( 10, ( attack * pow( defense, 0.5 ) * pow( stamina, 0.5 ) * pow( cpm, 2 ) ) / 10 ) );
}

export async function getCpMultiplierAsync( level: number ) {
    if ( !cpMultipliers ) {
        await loadCpMultipliersAsync();
    }

    return cpMultipliers[ level ] || 1.0;
}

async function loadCpMultipliersAsync() {
    let csvPath = path.join( __dirname, "../../data/cpmultiplier.csv" );
    let records = await readCsvAsync<{ level: number, cpm: number }>( csvPath, { cast: true } );

    cpMultipliers = {};

    for ( let record of records ) {
        cpMultipliers[ record.level ] = record.cpm;
    }
}