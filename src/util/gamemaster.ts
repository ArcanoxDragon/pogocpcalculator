import { promises as fs, Stats } from "fs";

import path from "path";
import { writeCsvAsync } from "@/util/csv";

// #region Type Definitions

interface GameMaster {
    itemTemplate: ItemTemplate[];
}

interface ItemTemplate {
    templateId: string;
    pokemon?: PokemonTemplate;
}

interface PokemonTemplate {
    uniqueId: string;
    type1: PokemonType;
    type2?: PokemonType;
    stats: PokemonStats;
}

type PokemonType =
    | "POKEMON_TYPE_NORMAL"
    | "POKEMON_TYPE_FIRE"
    | "POKEMON_TYPE_FIGHTING"
    | "POKEMON_TYPE_WATER"
    | "POKEMON_TYPE_FLYING"
    | "POKEMON_TYPE_GRASS"
    | "POKEMON_TYPE_POISON"
    | "POKEMON_TYPE_ELECTRIC"
    | "POKEMON_TYPE_GROUND"
    | "POKEMON_TYPE_PSYCHIC"
    | "POKEMON_TYPE_ROCK"
    | "POKEMON_TYPE_ICE"
    | "POKEMON_TYPE_BUG"
    | "POKEMON_TYPE_DRAGON"
    | "POKEMON_TYPE_GHOST"
    | "POKEMON_TYPE_DARK"
    | "POKEMON_TYPE_STEEL"
    | "POKEMON_TYPE_FAIRY";

interface PokemonStats {
    baseAttack: number;
    baseDefense: number;
    baseStamina: number;
}

// #endregion

export async function parseGameMasterFile() {
    let inPath = path.join( __dirname, "../../data/GAME_MASTER.json" );
    let statsOutPath = path.join( __dirname, "../../data/basestats.csv" );
    let gameMasterText = await fs.readFile( inPath, { encoding: "utf8" } );
    let gameMaster: GameMaster = JSON.parse( gameMasterText );

    type StatsRecord = { id: number, attack: number, defense: number, stamina: number };
    let statsRecords: StatsRecord[] = [];

    for ( let template of gameMaster.itemTemplate ) {
        if ( !template.pokemon ) {
            continue;
        }

        if ( !/^V\d{4}_POKEMON_.*$/.test( template.templateId ) ) {
            continue;
        }

        let templateMatch = template.templateId.match( /^V(\d{4})_POKEMON_(.*)$/ );

        if ( !templateMatch ) {
            continue;
        }

        let [ , idStr, templateName ] = templateMatch;

        if ( templateName !== template.pokemon.uniqueId ) {
            continue;
        }

        let { stats } = template.pokemon;
        let id = +idStr;

        console.log( `Found Pokémon ${id}: ${templateName}` );

        statsRecords.push( {
            id,
            attack: stats.baseAttack,
            defense: stats.baseDefense,
            stamina: stats.baseStamina,
        } );
    }

    // Write basestats file
    await writeCsvAsync( statsOutPath, statsRecords, {
        header: true,
        columns: [ "id", "attack", "defense", "stamina" ],
    } );
}