import { promises as fs } from "fs";

export async function fileExistsAsync( path: string ) {
    try {
        let stat = await fs.stat( path );

        if ( !stat ) {
            return false;
        }

        return stat.isFile;
    } catch {
        return false;
    }
}