import { promises as fs } from "fs";
import { fileExistsAsync } from "@/util/fsutil";

import parse, { Options as ParseOptions } from "csv-parse";
import stringify, { Options as StringifyOptions } from "csv-stringify";

export async function readCsvAsync<T>( path: string, options?: ParseOptions ) {
    options = options || {};

    let csvText = await fs.readFile( path, { encoding: "utf8" } );
    let parser = parse( csvText, {
        columns: true,
        ...options,
    } );
    let results: T[] = [];

    for await ( let record of parser ) {
        results.push( record as T );
    }

    return results;
}

export function writeCsvAsync<T>( path: string, records: T[], options?: StringifyOptions ) {
    return new Promise<void>( async ( res, rej ) => {
        options = options || {};


        if ( await fileExistsAsync( path ) ) {
            await fs.unlink( path );
        }

        let data: Buffer[] = [];
        let stringifier = stringify( options );

        stringifier.on( "error", rej );
        stringifier.on( "finish", async () => {
            for ( let line of data ) {
                await fs.appendFile( path, line, { encoding: "utf8" } );
            }

            res();
        } );
        stringifier.on( "readable", () => {
            let chunk: Buffer;

            while ( chunk = stringifier.read() ) {
                data.push( chunk );
            }
        } );

        for ( let record of records ) {
            stringifier.write( record );
        }

        stringifier.end();
    } );
}