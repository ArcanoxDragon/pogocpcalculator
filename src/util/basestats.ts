import { readCsvAsync } from "@/util/csv";
import { Basestats } from "@/models/Basestats";

import path from "path";

let baseStats: Record<number, Basestats>;

export async function getPokemonBasestatsAsync( id: number ) {
    if ( !baseStats ) {
        await loadBasestatsAsync();
    }

    return baseStats[ id ];
}

async function loadBasestatsAsync() {
    type Record = { id: number, attack: number, defense: number, stamina: number };
    let csvPath = path.join( __dirname, "../../data/basestats.csv" );
    let records = await readCsvAsync<Record>( csvPath, { cast: true } );

    baseStats = {};

    for ( let record of records ) {
        let { attack, defense, stamina } = record;

        baseStats[ record.id ] = { attack, defense, stamina };
    }
}