import { readCsvAsync } from "@/util/csv";

import path from "path";

let idToName: Record<number, string>;
let nameToId: Record<string, number>;

export async function getPokemonNameAsync( id: number ) {
    if ( !idToName ) {
        await loadPokenamesAsync();
    }

    return idToName[ id ];
}

export async function getPokemonIdAsync( name: string ) {
    if ( !nameToId ) {
        await loadPokenamesAsync();
    }

    return nameToId[ name.toLowerCase() ] || -1;
}

async function loadPokenamesAsync() {
    let csvPath = path.join( __dirname, "../../data/pokenames.csv" );
    let records = await readCsvAsync<{ id: number, name: string }>( csvPath, { cast: true } );

    idToName = {};
    nameToId = {};

    for ( let record of records ) {
        idToName[ record.id ] = record.name;
        nameToId[ record.name.toLowerCase() ] = record.id;
    }
}