const path = require( "path" );

require( "tsconfig-paths/register" );
require( "ts-node" ).register( {
    files: true,
    project: path.join( __dirname, "../tsconfig.json" ),
} );