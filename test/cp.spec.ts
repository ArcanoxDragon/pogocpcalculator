import { expect } from "chai";
import { getPokemonIdAsync } from "@/util/pokenames";
import { getPokemonBasestatsAsync } from "@/util/basestats";
import { Pokestats } from "@/models/Pokestats";
import { getCpAsync } from "@/util/cp";

describe( "Test Various Pokémon CPs", () => {
    async function getPokemonCpAsync( name: string, ivAtk: number, ivDef: number, ivSta: number, level: number ) {
        let id = await getPokemonIdAsync( name );
        let basestats = await getPokemonBasestatsAsync( id );
        let pokestats: Pokestats = {
            attackIv: ivAtk,
            defenseIv: ivDef,
            staminaIv: ivSta,
            level,
        };

        return await getCpAsync( basestats, pokestats );
    }

    function testMon( name: string, expectedCp: number, ivAtk: number, ivDef: number, ivSta: number, level: number ) {
        it( `Should return ${expectedCp} for ${ivAtk}/${ivDef}/${ivSta} lv${level} ${name}`, async () => {
            let cp = await getPokemonCpAsync( name, ivAtk, ivDef, ivSta, level );

            expect( cp ).to.equal( expectedCp );
        } );
    }

    testMon( "Gengar", 2300, 11, 6, 6, 30 );
    testMon( "Terrakion", 3159, 15, 13, 12, 30.5 );
    testMon( "Salamence", 3651, 15, 13, 12, 39 );
} )